import express from "express"
import LeadController from "../controllers/LeadController"
import Basic from "../middlewares/Basic"

const router = express.Router()
router.post('/lead', Basic, LeadController.create)
router.get('/leads', Basic, LeadController.list)
router.get('/lead/:id', Basic, LeadController.retrieve)
router.put('/lead/:id', Basic, LeadController.update)
router.delete('/lead/:id', Basic, LeadController.delete)

export default router