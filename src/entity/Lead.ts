import {Entity, ObjectIdColumn, Column, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {IsEmail, IsNotEmpty, IsPhoneNumber} from 'class-validator';

@Entity()
export class Lead {

    @ObjectIdColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    nome: string;

    @Column()
    @IsNotEmpty()
    cognome: string;

    @Column()
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @Column()
    @IsNotEmpty()
    @IsPhoneNumber('IT')
    telefono: string;

    @Column()
    @IsNotEmpty()
    privacy: boolean;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;
}