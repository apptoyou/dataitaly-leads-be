import { config } from "dotenv"
config()

export default function Basic(req, res, next) {


    const APP_TOKEN = process.env.APP_TOKEN

    if (req.hasOwnProperty('headers') && req.headers.hasOwnProperty('authorization')) {

        const bearerHeader = req.headers.authorization;
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];

        if (bearerToken == APP_TOKEN) {
            next();
        } else {
            return res.notAuthorized()
        }
    } else {
        return res.notAuthorized()
    }
};