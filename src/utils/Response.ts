import * as express from 'express'

export default class JsonResponses {
    
    /**
     * Decora il payload di risposta come schema interno
     * Il metodo può essere sovrascritto per modificare il comportamento.
     * @param {Object} payload 
     * @param {String} message 
     * @param {Boolean} errorState 
     * @param {String} code 
     */
    static wrapBody (payload, message, errorState, code) {
        const resp:Object = {meta: { message: message, error: errorState, code: code, version: "0.1" }}
        if (errorState) {
            resp['reason'] = payload
        } else {
            resp['data'] = payload
        }
        return resp
    }

    /**
     * Se chiamata estende le risposte di express.js
     */
    static extend () {
        const res:express.Response = express.response
        const wrapResponsePayload = this.wrapBody
        /**
         * STATUS: 200
         */
        res['success'] = function (payload, message="Success", code="OK_SUCCESS") {
            const wrapped = wrapResponsePayload(payload, message, false, code)
            return this.status(200).json(wrapped)
        }
        /**
         * STATUS: 201
         */
        res['created'] = function (payload, message="Resource created", code="OK_CREATED") {
            const wrapped = wrapResponsePayload(payload, message, false, code)
            return this.status(201).json(wrapped)
        }
        /**
         * STATUS 2014
         */
        res['noContent'] = function (message="No response data provided.", code="OK_NO_CONTENT") {
            this.set({
                'X-App-Message': message,
                'X-App-Error-State': false,
                'X-App-Version': process.env.APP_VERSION,
                'X-App-Meta-Code': code
            })
            return this.sendStatus(204)
        }
        /**
         * STATUS: 400
         */
        res['badRequest'] = function (reason="Wrong client request", message="Bad request.", code="KO_BAD_REQUEST") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(400).json(wrapped)
        }
        /**
         * STATUS: 401
         */
        res['notAuthorized'] = function (reason="Authentication error", message="Unauthorized.", code="KO_UNAUTHORIZED") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(401).json(wrapped)
        }
        /**
         * STATUS: 403
         */
        res['forbidden'] = function (reason="Action not permitted", message="Forbidden.", code="KO_FORBIDDEN") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(403).json(wrapped)
        }
        /**
         * STATUS: 404
         */
        res['notFound'] = function (reason="Resource not found", message="Not found.", code="KO_NOT_FOUND") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(404).json(wrapped)
        }
        /**
         * STATUS: 409
         */
        res['conflict'] = function (reason="Resource Conflict", message="Conflict.", code="KO_CONFLICT") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(409).json(wrapped)
        }
        /**
         * STATUS: 422
         */
        res['fieldError'] = function (reason="Cannot process request due to field error.", message="Field error.", code="KO_FIELD_ERROR") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(422).json(wrapped)
        }
        /**
         * STATUS: 405
         */
        res['methodNotAllowed'] = function (reason="This method is not allowed on this endpoint.", message="Not allowed.", code="KO_METHOD_NOT_ALLOWED") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(405).json(wrapped)
        }
        /**
         * STATUS: 500
         */
        res['errCantPerform'] = function (reason="Cannot perform action", message="Internal server error.", code="KO_CANT_PERFORM") {
            const wrapped = wrapResponsePayload(reason, message, true, code)
            return this.status(500).json(wrapped)
        }
    }
}