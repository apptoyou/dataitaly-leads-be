import { Lead } from './../entity/Lead';
import { getMongoManager } from "typeorm"
import { validate } from "class-validator"

const LeadController = {
    create: async (request, response) => {
        const body = request.body
        const leadRepository = getMongoManager().getMongoRepository(Lead)
        let createResource = null
        let saveResource = null
        try {
            createResource = await leadRepository.create(body)
            const errors = await validate(createResource)
            console.log(errors)
            if (errors.length > 0) {
                return response.badRequest(errors)
            } else {
                saveResource = await leadRepository.save(createResource)
            }
        } catch (err) {
            console.log(err.message)
        }
        return response.success(saveResource)
    },
    update: async (request, response) => {
        const body = request.body
        const leadRepository = getMongoManager().getMongoRepository(Lead)
        let saveResource = null
        try {
            const lead = await leadRepository.findOne(request.params.id);
            const resource = leadRepository.merge(lead, body);
            const errors = await validate(resource)
            console.log(errors)
            if (errors.length > 0) {
                return response.badRequest(errors)
            } else {
                saveResource = await leadRepository.save(resource)
            }
        } catch (err) {
            console.log(err.message)
        }
        return response.success(saveResource)
    },
    list: async (request, response) => {
        const leadRepository = getMongoManager().getMongoRepository(Lead)
        let result = []
        try {
            result = await leadRepository.find()
        } catch (err) {
            return response.errCantPerform(err.message)
        }
        return response.success(result)

    },
    retrieve: async (request, response) => {
        const leadRepository = getMongoManager().getMongoRepository(Lead)
        let result = {}
        try {
            result = await leadRepository.findOne(request.params.id)
        } catch (err) {
            return response.errCantPerform(err.message)
        }
        return response.success(result)

    },
    delete: async (request, response) => {
        const leadRepository = getMongoManager().getMongoRepository(Lead)
        let results = null
        try {
            results = await leadRepository.delete(request.params.id);
        } catch (err) {
            console.log(err.message)
        }
        return response.success(results);

    }
}

export default LeadController