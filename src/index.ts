import express from "express"
import { createConnection } from 'typeorm';
import {json, urlencoded} from "body-parser"
import routes from "./routes/routes"
import Response from "./utils/Response"
const app = express()
Response.extend() 

app.use('/', [json(), urlencoded({extended: false})])
app.get('/', function (req, res) {
  res.send('Hello World')
})
app.use('/api/v1', routes)

app.listen(process.env.PORT, () => {
  console.log('Listening on ' + process.env.PORT)
  createConnection({
    "type": "mongodb",
    "host": "localhost",
    "port": 27017,
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "useNewUrlParser": true,
    "synchronize": true,
    "logging": true,
    "useUnifiedTopology": true,
    "entities": ["build/entity/*.js"]
})
  .then(res => {
    console.log('Connection mongoDB')
  })
  .catch(err => {
    console.error(err)
  })
})